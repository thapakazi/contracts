var Roles = artifacts.require('Roles')
var TokenData = artifacts.require('TokenData')
var SweetToken = artifacts.require('SweetToken')
var SweetTokenLogic = artifacts.require('SweetTokenLogic')
var target = require('./minting.json')
var fs = require('fs')

module.exports = function (callback) {
  // let args = process.argv
  let swc = undefined
  let swl = undefined
  SweetToken.new('SweetToken', 'SWC', Roles.address)
    .then(st => {
      swc = st
      return SweetTokenLogic.new(swc.address, 0, Roles.address, target.owners, target.amounts)
    })
    .then(sl => {
      swl = sl
      return swc.setLogic(swl.address)
    })
    .then(() => swl.data())
    .then(dataAddr => {
      let data = TokenData.at(dataAddr)
      return Promise.all([
        data.setOwner(target.contractOwner),
        swc.setOwner(target.contractOwner),
        swl.setOwner(target.contractOwner)
      ])
    })
    .then(() => {
      fs.writeFileSync('swc.json', JSON.stringify({swc: swc.address, swcLogic: swl.address}))
      callback()
    })
}
