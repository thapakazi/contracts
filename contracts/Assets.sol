// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pragma solidity ^0.4.23;

import "./tokens/ERC20.sol";
import "./authority/Roles.sol";


// AssetsLib provides funcitonality for managing assets which we represent as
// an ERC20 list
library AssetsLib {
    function assetsLen(ERC20[] storage self) public view returns(uint32) {
        return uint32(self.length);
    }

    // @returns array of token addresses and array of associated balances.
    function balances(ERC20[] storage self) internal view returns(address[], uint256[]) {
        address[] memory addrs = new address[](self.length);
        uint256[] memory bs = new uint256[](self.length);
        for (uint32 i = 0; i < self.length; ++i) {
            addrs[i] = address(self[i]);
            bs[i] = self[i].balanceOf(this);
        }
        return (addrs, bs);
    }

    function hasFunds(ERC20[] storage self) public view returns(bool nonZero) {
        for (uint32 i = 0; i < self.length; ++i) {
            nonZero = self[i].balanceOf(this) > 0 || nonZero;
        }
        return nonZero;
    }

    // @wad - the amount of tokens to add. Can be 0 - the motivation is to enable
    //   the accountant to see the tokens which he should manage
    function addAsset(
        ERC20[] storage self,
        ERC20 token,
        address src,
        uint256 wad) public
    {
        // limit the number of assets to avoid an OOG during operations
        if (wad > 0)
            require(src != address(0x0));
        var (, ok) = indexOf(self, token);
        if (!ok) {
            require(self.length < 255);
            self.push(token);
        }
        if (wad > 0)
            token.transferFrom(src, this, wad);
    }

    function rmAsset(ERC20[] storage self, ERC20 token, address dst) public returns(bool) {
        require(dst != address(0));
        var (i, ok) = indexOf(self, token);
        if (ok) {
            self[i].transfer(dst, self[i].balanceOf(this));
            rmAssetIdx(self, i);
            return true;
        }
        return false;
    }

    // returns the first index at which a given element can be found in the storage,
    // or -1 if it is not present.
    function indexOf(ERC20[] storage self, ERC20 token) public view returns(uint32, bool) {
        for (uint32 i = 0; i < self.length; ++i) {
            if (self[i] == token)
                return (i, true);
        }
        return (0, false);
    }

    function rmAssetIdx(ERC20[] storage self, uint32 idx) private {
        uint32 len = uint32(self.length) - 1;
        if (idx != len)
            self[idx] = self[len];
        delete self[len];
        self.length = len;
    }

    // Removes empty entries in assets storage
    function cleanStorage(ERC20[] storage self) public {
        uint32 len = uint32(self.length) - 1;
        for (uint32 i = len; i >= 0; --i) {
            if (self[i].balanceOf(this) == 0 ) {
                if (i != len) {
                    self[i] = self[len];
                }
                delete self[len];
                --len;
            }
        }
        self.length = len;
    }
}


contract AssetEvents {
    event AssetAdded(address token, address owner, uint256 wad);
    event AssetRemoved(address token);
}


contract Assets is SecuredWithRoles, AssetEvents {
    using AssetsLib for ERC20[];
    ERC20[] public assets;

    constructor(address[2] defaultTokens, string contractName, address rolesContract) SecuredWithRoles(contractName, rolesContract) public {
        for (uint32 i = 0; i < defaultTokens.length; i++) {
            if (defaultTokens[i] != address(0x0)) {
                addAsset(ERC20(defaultTokens[i]), address(0x0), 0);
            }
        }
    }

    function () public payable {}

    function tokenFallback(address, uint, bytes) public {}

    function assetsLen() public view returns(uint256) {
        return assets.assetsLen();
    }

    function balanceOf(ERC20 token) public view returns(uint256) {
        return token.balanceOf(this);
    }

    function ethBalance() public view returns(uint256) {
        return address(this).balance;
    }

    function balances() public view returns(address[], uint256[]) {
        return assets.balances();
    }

    function hasFunds() public view returns(bool) {
        return assets.hasFunds() || address(this).balance > 0;
    }

    function addAsset(ERC20 token, address src, uint256 wad) public roleOrOwner("assetManager") {
        assets.addAsset(token, src, wad);
        emit AssetAdded(token, src, wad);
    }

    function transfer(ERC20 token, address dst, uint256 wad) stoppable public onlyOwner {
        token.transfer(dst, wad);
    }

    function transferEth(address dst, uint256 wad) stoppable public onlyOwner {
        dst.transfer(wad);
    }

    // @dst - address where the outstanding balance should be transferred.
    function rmAsset(ERC20 token, address dst) public onlyOwner stoppable returns(bool) {
        if (assets.rmAsset(token, dst)) {
            emit AssetRemoved(token);
            return true;
        }
        return false;
    }

    function cleanStorage() public roleOrOwner("assetManager") {
        assets.cleanStorage();
    }
}
