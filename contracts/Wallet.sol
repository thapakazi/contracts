// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pragma solidity ^0.4.23;

import "./LockedAssets.sol";
import "./Vault.sol";
import "./tokens/Token.sol";


contract WalletI {
    function treasuryAddress() view public returns (address);
    function roles() public view returns (address);
    function owner() public view returns (address);
    function brg() public view returns (TokenI);
    function swc() public view returns (TokenI);
    function currency() public view returns (bytes3);
    function removeVault(address) public;
}

contract WalletEvents {
    event LogVaultAdded(address vault);
    event LogVaultRemoved(address vault);
}


/*Wallet belongs to exactly one user (might be a multi-sig) who is the owner of the contract.
  Use `addAsset` (from Assets) function to transfer assets to the wallet. After this operation
  wallet will become the owner of the assets.
  To move the token to another address use `transfer` function. */
contract Wallet is LockedAssets, WalletEvents {
    bytes3 public currency; // the ISO code of the currency we want to evaluate assets in
    uint32 public vaultCount;
    address public brg;
    address public swc;
    VaultFactoryI vaultFactory;
    // TODO: the treasury address should be a parameter not a hard coded address
    address public treasuryAddress = 0xa6B8b75d85b6D2C22b8E9e2768F2F405CF09005A;

    /*The Wallet contract must be made a VaultCreator after its creation for it to be able to use the VaultFactory*/
    constructor(address owner_, address roles_, bytes3 currency_, address swc_, address brc_, address vaultFactory_)
    LockedAssets([swc_, brc_], "Wallet", roles_) public
    {
        owner = owner_;
        currency = currency_;
        brg = brc_;
        swc = swc_;
        vaultCount = 0;
        vaultFactory = VaultFactoryI(vaultFactory_);
    }

    function remove() public onlyOwner {
        require(address(this).balance == 0);
        require(!hasFunds());
        require(vaultCount == 0);
        selfdestruct(address(0));
    }

    function addVault() public roleOrOwner("graphQl") {
        address vault = vaultFactory.createVault(this);
        vaultCount++;
        emit LogVaultAdded(vault);
    }

    // can only be called by the VaultFactory itself
    function removeVault(address vault_) public {
        require(msg.sender == address(vaultFactory));
        vaultCount--;
        emit LogVaultRemoved(vault_);
    }
}
