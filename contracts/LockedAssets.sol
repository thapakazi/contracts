// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


pragma solidity ^0.4.23;

import "./Assets.sol";
import "./tokens/Token.sol";


contract LockedAssetsEvents {
    event AssetRemovalRequest(address token);
    event AssetRemovalRequestApproved(address token);
    event AssetRemovalRequestRejected(address token);
    event AssetTransferRequest(address token, uint256 wad);
    event AssetTransferRequestApproved(address token, uint256 wad);
    event AssetTransferRequestRejected(address token, uint256 wad);
}

contract LockedAssets is Assets, LockedAssetsEvents {
    enum RequestType {initial, transfer, removal}
    enum RequestState {initial, pending, executed, rejected}

    struct TransferRequest {
        ERC20 token;
        address dst;
        uint256 wad;
        RequestState state;
    }

    struct RemovalRequest {
        ERC20 token;
        address dst;
        RequestState state;
    }

    TransferRequest public transferRequest;
    RemovalRequest public removalRequest;

    constructor(address[2] defaultTokens, string contractName, address rolesContract)
    Assets(defaultTokens, contractName, rolesContract) public
    {}

    function transfer(ERC20 token, address dst, uint256 wad) stoppable public onlyOwner {
        // if a request is pending, no new request is accepted
        require(transferRequest.state != RequestState.pending);
        //make sure that this is a legitimate request
        require(token.balanceOf(this) >= wad);

        transferRequest.token = token;
        transferRequest.dst = dst;
        transferRequest.wad = wad;
        transferRequest.state = RequestState.pending;
        emit AssetTransferRequest(token, wad);
    }

    function transferEth(address dst, uint256 wad) stoppable public onlyOwner {
        // if a request is pending, no new request is accepted
        require(transferRequest.state != RequestState.pending);

        transferRequest.token = ERC20(address(0x0));
        transferRequest.dst = dst;
        transferRequest.wad = wad;
        transferRequest.state = RequestState.pending;
        emit AssetTransferRequest(address(0x0), wad);
    }

    // Assets can only be removed directly if the balance of the asset is zero
    function rmAsset(ERC20 token, address dst) public onlyOwner stoppable returns (bool) {
        // check that the vault contains the asset before submitting the request
        var (, ok) = assets.indexOf(token);
        require(ok);

        if (token.balanceOf(this) == 0) {
            Assets.rmAsset(token, dst);
        } else {
            // if a request is pending, no new request is accepted
            require(removalRequest.state == RequestState.pending);
            removalRequest.token = token;
            removalRequest.dst = dst;
            removalRequest.state = RequestState.pending;
            emit AssetRemovalRequest(token);
        }
    }

    /**
    either accept and execute the request or reject an close it
    in case the accept transaction fails, it is the responsibility of the oracle to reject the request in order
    to unlock the vault or wallet
    TODO: https://jira.sweetbridge.com/browse/DIAM-607 handle the error case and attack scenario
    */
    function resolveRequest(RequestType requestType, RequestState decision) public onlyRole("transferOracle") {
        if(requestType == RequestType.transfer) {
            require (transferRequest.state == RequestState.pending);
            transferRequest.state = decision;
            if(decision == RequestState.executed){
                //the function for transfer or ethTransfer can not be used because they are callable only by the owner
                if(transferRequest.token == address(0x0)) {
                    transferRequest.dst.transfer(transferRequest.wad);
                } else {
                    transferRequest.token.transfer(transferRequest.dst, transferRequest.wad);
                }
                emit AssetTransferRequestApproved(transferRequest.token, transferRequest.wad);
            } else {
                emit AssetTransferRequestRejected(transferRequest.token, transferRequest.wad);
            }
        } else if(requestType == RequestType.removal) {
            require (removalRequest.state == RequestState.pending);
            removalRequest.state = decision;
            if(decision == RequestState.executed){
                Assets.rmAsset(removalRequest.token, removalRequest.dst);
                emit AssetRemovalRequestApproved(removalRequest.token);
            } else {
                emit AssetRemovalRequestRejected(removalRequest.token);
            }
        }
    }
}
