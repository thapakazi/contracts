// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* global web3 */

let utils = require('.//utils')
var TokenLogic = artifacts.require('TokenLogic')
var SweetTokenLogic = artifacts.require('SweetTokenLogic')
var SWT = artifacts.require('SweetToken')
var BRG = artifacts.require('BridgeToken')
var Roles = artifacts.require('Roles')

module.exports = function (deployer, network) {
  // logic has to be deployed separaterly because Token shouldn't be the owner of the logic

  var swt, brg
  const accounts = web3.eth.accounts
  let roles = Roles.at(Roles.address)

  function deployBrg () {
    return deployer.deploy(BRG, 'BRG', 'BRG', Roles.address)
      .then(() => BRG.deployed())
      .then(b => {
        brg = b
        return deployer.deploy(TokenLogic, brg.address, 0, roles.address)
      })
      .then(() => brg.setLogic(TokenLogic.address))
      .then(() => utils.setRole(brg, roles, 'admin'))
      .then(() => utils.setRole(brg, roles, 'minter'))
  }

  function deploySwt () {
    return deployer.deploy(SWT, 'SWT', 'SWT', roles.address)
      .then(() => SWT.deployed())
      .then(s => {
        swt = s
        return deployer.deploy(
          SweetTokenLogic, swt.address, 0, roles.address,
          [accounts[0], accounts[12], accounts[13], accounts[14], accounts[15]],
          [1e25, 1e25, 2e25, 3e25, 3e25])
      })
      .then(() => swt.setLogic(SweetTokenLogic.address))
      .then(() => utils.setRole(swt, roles, 'admin'))
  }

  return deployBrg()
    .then(() => deploySwt())
    .catch(console.log)
}
