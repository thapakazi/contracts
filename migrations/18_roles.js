// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var Roles = artifacts.require('Roles')
var VaultFactory = artifacts.require('VaultFactory')

module.exports = function (deployer) {
  let roles
  let vaultFactory
  let rHash
  let vfHash

  return deployer.deploy(Roles)
    .then(() => Roles.deployed())
    .then(r => {
      roles = r
      return deployer.deploy(VaultFactory, roles.address)
    })
    .then(() => VaultFactory.deployed())
    .then(vf => {
      vaultFactory = vf
      return Promise.all([roles.contractHash(), vaultFactory.contractHash()])
    })
    .then(res => {
      rHash = res[0]
      vfHash = res[1]
      return roles.addContractRole(rHash, 'admin')
    })
    .then(() => roles.grantUserRole(rHash, 'admin', vaultFactory.address))
    .then(() => roles.addContractRole(vfHash, 'vaultCreator'))
}
