// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const fs = require('fs')

const VaultFactory = artifacts.require('VaultFactory')
const Roles = artifacts.require('Roles')
const BRG = artifacts.require('BridgeToken')
const Wallet = artifacts.require('Wallet')
const Vault = artifacts.require('Vault')
const SWT = artifacts.require('SweetToken')
const BN = require('bignumber.js')
let helpers = require('./helpers.js')
let utils = require('./utils')

const getNumber = async (fn, ...params) => (await fn(...params)).toNumber()

contract('Vault', function (accounts) {
  const RequestType = {initial: 0, transfer: 1, removal: 2}
  const RequestState = {initial: 0, pending: 1, executed: 2, rejected: 3}
  let vaultFactory
  let roles
  let vault
  let wallet
  let brg
  let swt
  let user = accounts[4]
  let uouOracle = accounts[7]
  let treasuryOracle = accounts[8]
  const fscb = function () { /* nothing to do in fs callback */ }

  before(async () => {
    brg = await BRG.deployed()
    swt = await SWT.deployed()
    vaultFactory = await VaultFactory.deployed()
    roles = await Roles.deployed()
  })

  it('can create a wallet', async () => {
    wallet = await Wallet.new(user, Roles.address, 'USD', SWT.address, BRG.address, VaultFactory.address)
    await roles.grantUserRole(await vaultFactory.contractHash(), 'vaultCreator', wallet.address)
    assert.equal(await wallet.owner(), user)

    await wallet.addAsset(BRG.address, '0x0', 0, {from: user})
  })

  it('creates a new Vault in the Wallet', async () => {
    let tx = await wallet.addVault({from: user})
    fs.appendFile('gas.csv', 'Wallet;addVault;' + tx.receipt.gasUsed + '\n', fscb)
    let vaultAdded = tx.logs.find(log => log.event === 'LogVaultAdded').args
    vault = Vault.at(vaultAdded.vault)
    assert.equal(await wallet.owner(), await vault.owner())
    assert.ok(await brg.senderHasRole('minter', {from: vault.address}))
    utils.addRole('treasuryOracle', vault, treasuryOracle)
  })

  it('assigns roles to participants', async () => {
    await utils.addRole('uouOracle', vault, uouOracle)
    await utils.addRole('transferOracle', vault, uouOracle)
    await utils.addRole('transferOracle', wallet, uouOracle)
    assert.ok(await brg.senderHasRole('minter', {from: vault.address}))
    await utils.addRole('minter', brg, accounts[0])
  })

  it('has been initialized', async () => {
    const owner = await vault.owner()
    const due = await vault.balance()
    const walletAddress = await vault.wallet()
    assert.equal(owner, user)
    assert.equal(walletAddress, wallet.address)
    assert.equal(0, due.toNumber(), 'amount due')
  })

  it('removes the default SWT and BRG assets from the vault', () =>
    vault.assetsLen()
      .then(length => assert.equal(length.toNumber(), 2))
      .then(() => vault.assets(0))
      .then(asset => assert.equal(asset, SWT.address))
      .then(() => vault.rmAsset(SWT.address, accounts[1], {from: user}))
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;rmAsset;' + tx.receipt.gasUsed + '\n', fscb)
        assert.isOk(tx)
        let removed = tx.logs.find(log => log.event === 'AssetRemoved').args
        assert.equal(removed.token, SWT.address)
        return vault.rmAsset(BRG.address, accounts[1], {from: user})
      })
      .then(tx => {
        fs.appendFile('gas.csv', 'Wallet;rmAsset;' + tx.receipt.gasUsed + '\n', fscb)
        assert.isOk(tx)
        let removed = tx.logs.find(log => log.event === 'AssetRemoved').args
        assert.equal(removed.token, BRG.address)
      })
  )

  it('can not request a UOU before having funds', async () => {
    utils.assertThrowsAsynchronously(() => vault.requestUou(1e21, {from: user}))
  })

  // this test requires previous test execution
  it('can add SWC as an asset', async () => {
    await swt.transfer(user, 2e21, {from: accounts[12]})
    const swtBalance = await swt.balanceOf(user)

    const tx = await swt.approve(vault.address, 1e21, {from: user, gas: 1000000})
    await fs.appendFile('gas.csv', 'SweetToken;approve;' + tx.receipt.gasUsed + '\n', fscb)
    const vaultTx = await vault.addAsset(swt.address, user, 6e20, {from: user})
    await fs.appendFile('gas.csv', 'Vault;addAsset;' + vaultTx.receipt.gasUsed + '\n', fscb)

    const len = (await vault.assetsLen()).toNumber()
    const asset0 = await vault.assets(0)

    const balance = (await vault.balanceOf(SWT.address)).toNumber()
    const vaultAddr = (await swt.balanceOf(vault.address)).toNumber()
    const usrBalance = (await swt.balanceOf(user)).toNumber()

    assert.equal(len, 1)  // check assets len
    assert.equal(asset0, SWT.address)
    assert.equal(balance, 6e20)
    assert.equal(vaultAddr, 6e20)
    assert.equal(usrBalance, swtBalance.minus(6e20))  // 1e22 = initial SWT supply
  })

  it('refers a token transfer request to the oracle', async () => {
    let startBalance = await swt.balanceOf(user)
    let vaultStartBalance = await swt.balanceOf(vault.address)
    let tx = await vault.transfer(swt.address, user, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(vault.address)).toNumber())

    tx = await vault.resolveRequest(RequestType.transfer, RequestState.executed, {from: uouOracle})
    let requestApproval = tx.logs.find(log => log.event === 'AssetTransferRequestApproved')
    assert.ok(requestApproval)

    assert.equal(startBalance.add(1e18).toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.sub(1e18).toNumber(), (await swt.balanceOf(vault.address)).toNumber())
    await swt.transfer(vault.address, 1e18, {from: user})
    assert.equal((await vault.balanceOf(SWT.address)).toNumber(), 6e20)

  })

  it('keeps the same balance when the transfer request is rejected', async () => {
    let startBalance = await swt.balanceOf(user)
    let vaultStartBalance = await swt.balanceOf(vault.address)
    let tx = await vault.transfer(swt.address, user, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(vault.address)).toNumber())

    tx = await vault.resolveRequest(RequestType.transfer, RequestState.rejected, {from: uouOracle})
    let requestRejection = tx.logs.find(log => log.event === 'AssetTransferRequestRejected')
    assert.ok(requestRejection)

    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(vault.address)).toNumber())
  })

  it('can add ETH as an asset', async () => {
    const ethBalance = await web3.eth.getBalance(user)
    assert.isAtLeast(ethBalance.toNumber(), 2e18, 'user must have 2e18 to allow access to them')
    let tx = await web3.eth.sendTransaction({to: vault.address, value: 2e18, from: user})

    const balance = (await web3.eth.getBalance(vault.address)).toNumber()

    assert.equal(balance, 2e18)
  })

  it('refers an ETH transfer request to the oracle', async () => {

    const startBalance = await web3.eth.getBalance(treasuryOracle)
    let vaultStartBalance = await web3.eth.getBalance(vault.address)
    let tx = await vault.transferEth(treasuryOracle, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await web3.eth.getBalance(treasuryOracle)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await web3.eth.getBalance(vault.address)).toNumber())

    tx = await vault.resolveRequest(RequestType.transfer, RequestState.executed, {from: uouOracle})
    let requestApproval = tx.logs.find(log => log.event === 'AssetTransferRequestApproved')
    assert.ok(requestApproval)

    assert.equal((await web3.eth.getBalance(vault.address)).toString(10), vaultStartBalance.sub(1e18).toString(10))
    assert.equal((await web3.eth.getBalance(treasuryOracle)).toString(10), startBalance.add(1e18).toString(10))
  })

  it('keeps the ETH balance unchanged if the transfer request is rejected', async () => {

    const startBalance = await web3.eth.getBalance(treasuryOracle)
    let vaultStartBalance = await web3.eth.getBalance(vault.address)
    let tx = await vault.transferEth(treasuryOracle, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await web3.eth.getBalance(treasuryOracle)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await web3.eth.getBalance(vault.address)).toNumber())

    tx = await vault.resolveRequest(RequestType.transfer, RequestState.rejected, {from: uouOracle})
    let requestRejection = tx.logs.find(log => log.event === 'AssetTransferRequestRejected')
    assert.ok(requestRejection)

    assert.equal((await web3.eth.getBalance(vault.address)).toString(10), vaultStartBalance.toString(10))
    assert.equal((await web3.eth.getBalance(treasuryOracle)).toString(10), startBalance.toString(10))
  })

  it.skip('does not increase the amount due if the UOU is rejected', async () => {
    let balance = await vault.balance()
    let tx = await vault.requestUou(1e21, {from: user})
    fs.appendFile('gas.csv', 'Vault;requestUou;' + tx.receipt.gasUsed + '\n', fscb)
    let uouRequested = tx.logs.find(log => log.event === 'UouRequested')
    assert.ok(uouRequested)
    assert.equal(uouRequested.args.brgAmount.toNumber(), 1e21)
    tx = await vault.rejectUouRequest({from: uouOracle})
    fs.appendFile('gas.csv', 'Vault;rejectUouRequest;' + tx.receipt.gasUsed + '\n', fscb)
    const uouRequestDeclined = tx.logs.find(log => log.event === 'UouRequestDeclined')
    assert.ok(uouRequestDeclined)
    assert.equal(uouRequestDeclined.args.brgAmount.toNumber(), 1e21)
    assert.equal((await vault.balance()).toNumber(), balance.toNumber())
  })

  it.skip('can request UOU for 1\'000 BRG', async () => {
    const tx = await vault.requestUou(1e21, {from: user})
    await fs.appendFile('gas.csv', 'Vault;requestUou;' + tx.receipt.gasUsed + '\n', fscb)
    const uouRequested = tx.logs.find(log => log.event === 'UouRequested')
    const due = await vault.balance()
    assert.ok(uouRequested)
    assert.equal(uouRequested.args.brgAmount.toNumber(), 1e21)
    assert.equal(due.toNumber(), 0)
  })

  it.skip('adds the UOU amount to the amount due in case of acceptance', async () => {
    const gamma = (new BN(1 + 0.06 / 12)).toPower(24).sub(1)
    let fee = new BN(1e21).mul(gamma)
    assert.ok(await vault.senderHasRole('uouOracle', {from: uouOracle}))
    assert.isAtLeast((await vault.requestedAmount()), 1e21)
    let tx = await vault.acceptUouRequest(fee, {from: uouOracle})
    fs.appendFile('gas.csv', 'Vault;acceptUouRequest;' + tx.receipt.gasUsed + '\n', fscb)
    const uouRequestApproval = tx.logs.find(({event}) => event === 'UouRequestApproved')
    assert.ok(uouRequestApproval)
    assert.equal(uouRequestApproval.args.brgAmount.toNumber(), 1e21)
    assert.isAbove(fee.toNumber(), 0)
    assert.equal(uouRequestApproval.args.feeAmount.toNumber(), fee.toNumber())
    let balance = await brg.balanceOf(wallet.address)
    assert.equal(balance.toNumber(), 1e21)

    let due = await vault.balance()
    assert.equal(due.sub(fee).toNumber(), 1e21)
    due = await vault.balance()
    assert.equal(due.sub(fee).toNumber(), 1e21, 'amountDue')
    assert.equal((await vault.fee()).toNumber(), fee.toNumber(), 'fee')
  })

  it.skip('transfers the BRG to the user', async () => {
    wallet.transfer(BRG.address, user, 1e21, {from: user})
    let tx = await wallet.resolveRequest(RequestType.transfer, RequestState.executed, {from: uouOracle})
    assert.equal((await brg.balanceOf(user)).toNumber(), 1e21)
  })

  it.skip('adds fees to the UOU and updates the amountDue', async () => {
    const expectedAmountDue = 1.1433293848899971e21
    const startingFees = await vault.fee()
    const startingAmount = await vault.balance()
    const addFeeTx = await vault.setBalance(
      startingAmount.minus(startingFees).toString(10),
      startingFees.add(1e18).toString(10),
      {from: treasuryOracle}
    )
    const due = await vault.balance()
    const endingFees = await vault.fee()
    const amountAfterFees = await vault.balance()

    assert.equal(due.toNumber(), startingAmount.add(1e18).toNumber(), 'The difference in amount should equal the difference in fees')
    assert.equal(amountAfterFees.toNumber(), startingAmount.add(1e18).toNumber(), `After adding the new fees the amount due ${amountAfterFees} should be the starting amount${startingAmount} plus fee`)
  })

  it.skip('fails if the same UOU is settled a second time', () =>
    utils.assertThrowsAsynchronously(() => vault.acceptUouRequest({from: uouOracle}))
  )

  it.skip('forwards any BRG it gets to the treasury', async () => {
    const treasury = await vault.treasuryAddress()
    const treasBal = await brg.balanceOf(treasury)
    const userBal = await brg.balanceOf(user)
    assert.isAtLeast(userBal.toNumber(), 2e20, 'user must have 2e20 BRG to send them')
    let tx = await brg.transfer(vault.address, 2e20, {from: user, gas: 1000000})

    assert.equal(treasBal.add(2e20).toNumber(), (await brg.balanceOf(treasury)).toNumber())
  })

  it.skip('repays part of a UOU', async () => {
    const beginningBalance = await vault.balance()
    const treasury = await vault.treasuryAddress()
    let tx = await brg.transfer(vault.address, 3e20, {from: user})
    await fs.appendFile('gas.csv', 'BridgeToken;repayUou;' + tx.receipt.gasUsed + '\n', fscb)
    const [treasuryTransfer, vaultTransfer] = tx.logs.filter(log => log.event === 'Transfer')
    await vault.setBalance(beginningBalance.sub(5e20).toNumber(), 0, {from: treasuryOracle})
    const balance = await vault.balance()

    assert.ok(vaultTransfer)
    assert.equal(vaultTransfer.args.from, user)
    assert.equal(vaultTransfer.args.to, vault.address)
    assert.equal(vaultTransfer.args.value.toNumber(), 3e20)
    assert.equal(treasuryTransfer.args.value.toNumber(), 3e20)
    assert.equal((await vault.fee()).toNumber(), 0)
    assert.equal(balance.toNumber(), beginningBalance.sub(5e20).toNumber(), 'amountDue')
    // the user need some BRG if they are to repay the UOU
    const mintTx = await brg.mintFor(user, 5e21)
    await fs.appendFile('gas.csv', 'BridgeToken;mintFor;' + mintTx.receipt.gasUsed + '\n', fscb)
  })

  it.skip('repays total of a UOU even though more is sent', async () => {
    const treasury = await vault.treasuryAddress()
    const initialDue = await vault.balance()
    const initBrcBalance = await brg.balanceOf(user)
    const txAmount = initialDue.add(2e21)
    assert(initBrcBalance.toNumber(), 2e21, 'should have more than outstanding balance')
    const tx = await brg.transfer(vault.address, txAmount.toNumber(), {from: user})
    await fs.appendFile('gas.csv', 'BridgeToken;repayUou;' + tx.receipt.gasUsed + '\n', fscb)

    await vault.setBalance(0, 0, {from: treasuryOracle})
    await brg.transfer(user, txAmount.minus(initialDue).toString(10), {from: treasury})
    const transfer = tx.logs.find(log => log.event === 'Transfer')

    const finalDue = await vault.balance()
    const finalBrcBalance = await brg.balanceOf(user)

    assert.ok(transfer)
    assert.equal(initBrcBalance.toNumber(), finalBrcBalance.add(initialDue).toNumber())
    assert.equal(finalDue.toNumber(), 0, 'amountDue')
  })

})
