// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* global web3 */

var helpers = require('./helpers.js')
var SweetToken = artifacts.require('SweetToken')
var SweetTokenLogic = artifacts.require('SweetTokenLogic')

contract('SweetToken', function (accounts) {
  let swt, newLogic, initialBalance, supply
  const fs = require('fs')
  const fscb = function () { /* nothing to do in fs callback */ }

  before(() => {
    return SweetToken.deployed()
      .then(t => (swt = t))
  })

  it('has an admin role', () => {
    return swt.hasRole('admin')
      .then(hasAdmin => assert.ok(hasAdmin))
  })

  it('has minted coins to 4 accounts', () => {
    return Promise.all([
      swt.balanceOf(accounts[0]),
      swt.balanceOf(accounts[12]),
      swt.balanceOf(accounts[13]),
      swt.balanceOf(accounts[14]),
      swt.balanceOf(accounts[15])
    ])
      .then(res => {
        assert.equal(res[0], 1e25)
        assert.equal(res[1], 1e25)
        assert.equal(res[2], 2e25)
        assert.equal(res[3], 3e25)
        assert.equal(res[4], 3e25)
      })
  })

  it('can not mint new tokens - even from owner', () => {
    return swt.totalSupply()
      .then(ts => {
        supply = ts.toNumber()
        assert.equal(ts.toNumber(), 1e26)
        return swt.mintFor(accounts[0], 1e18)
      })
      .then(() => assert.fail('Previous statement should fails'))
      .catch(err => assert(err.message.indexOf('transaction: revert') >= 0,
        'assertion error on mint ' + err.message + ' ' + swt.address))
  })

  it('can not burn tokens', () => {
    return swt.burn(1e17)
      .then((x) => {
        console.log('%%%%%%%%%%%%', x)
        assert.fail('Previous statement should fails')
      }).catch(err => {
        assert(err.message.indexOf('transaction: revert') >= 0, err)
      })
  })

  it('does not accept ether', () => {
    return helpers.sendTransaction(
      {from: accounts[0], to: swt.address, value: web3.toWei(1, 'ether')})
      .then(() => assert.fail('Previous statement should fails'))
      .catch(err => assert(err.message.indexOf('transaction: revert') >= 0,
        err.message))
  })

  it('can transfer sweetcoins', () => {
    let ownerBalance = 0
    return swt.balanceOf(accounts[15])
      .then(bal => {
        ownerBalance = bal
        assert(bal.toNumber() > 1e25)
        return swt.transfer(accounts[1], 1e25, {from: accounts[15]})
      })
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;transfer;' + tx.receipt.gasUsed + '\n', fscb)
        assert(tx, 'transaction returns true')
        return Promise.all([
          swt.balanceOf(accounts[15]),
          swt.balanceOf(accounts[1])
        ])
      })
      .then(res => {
        assert.equal(res[0].toNumber(), ownerBalance.sub(res[1]).toNumber(), 'src account balance has been updated')
        assert.equal(res[1].toNumber(), 1e25, 'src account balance has been updated')
        return swt.transfer(accounts[2], 5e24, {from: accounts[1]})
      })
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;transfer;' + tx.receipt.gasUsed + '\n', fscb)
        assert(tx, 'transaction returns true')
        return Promise.all([
          swt.balanceOf(accounts[1]),
          swt.balanceOf(accounts[2])
        ])
      })
      .then(res => {
        assert.equal(res[0].toNumber(), 5e24, 'accounts[1] balance has been updated')
        assert.equal(res[1].toNumber(), 5e24, 'accounts[2] balance has been updated')
        return true
      })
  })

  it('allows to pull - transferFrom tokens', () => {
    return swt.approve(accounts[1], 1e21, {from: accounts[15]})
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;approve;' + tx.receipt.gasUsed + '\n', fscb)
        return swt.allowance(accounts[15], accounts[1])
      })
      .then(allowance => assert.equal(allowance.toNumber(), 1e21))
      .then(() => swt.pull(accounts[15], 4e20, {from: accounts[1]}))
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;pull;' + tx.receipt.gasUsed + '\n', fscb)
        return swt.allowance(accounts[15], accounts[1])
      })
      .then(allowance => assert.equal(allowance.toNumber(), 6e20))
      .then(() => swt.transferFrom(accounts[15], accounts[6], 6e20, {from: accounts[1]}))
      .then(tx => {
        fs.appendFile('gas.csv', 'SweetToken;transferFrom;' + tx.receipt.gasUsed + '\n', fscb)
        return swt.balanceOf(accounts[6])
      })
      .then(balance => {
        assert.equal(6e20, balance.toNumber())
        return swt.allowance(accounts[15], accounts[1])
      })
      .then(allowance => assert.equal(allowance.toNumber(), 0))
  })

  it('can replace the logic contract', async () => {
    let swtLogic = SweetTokenLogic.at(await swt.logic())
    let rolesAddress = await swt.roles()
    let dataAddress = await swtLogic.data()
    let wallets = [accounts[1], accounts[2]]
    let balances = [1e22, 1e21]
    initialBalance = await swt.balanceOf(accounts[12])

    newLogic = await SweetTokenLogic.new(swt.address, dataAddress, rolesAddress, wallets, balances)
    let watcher = swt.LogLogicReplaced({fromBlock: 'latest'})
    await Promise.all([
      swtLogic.replaceLogic(newLogic.address),
      new Promise((resolve, reject) => {
        watcher.watch(function (error, result) {
          // This will catch all events, regardless of how they originated.
          assert.notOk(error)
          assert.equal(newLogic.address, result.args.newLogic)
          watcher.stopWatching()
          resolve(true)
        })
        setTimeout(() => {
          watcher.stopWatching()
          reject(new Error('timeout'))
        }, 2000)
      })
    ])
  })

  it('has a new logic address', () => {
    return swt.logic()
      .then(tl => {
        assert.equal(newLogic.address, tl)
      })
  })

  it('keeps the same total supply and owner balance after resetting the logic contract', () => {
    return swt.totalSupply()
      .then(ts => assert.equal(supply, ts.toNumber()))
      .then(() => swt.balanceOf(accounts[12]))
      .then(ib => assert.equal(initialBalance.toNumber(), ib.toNumber()))
  })
})
